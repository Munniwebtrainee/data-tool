import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveNullsAndDuplicatesComponent } from './remove-nulls-and-duplicates.component';

describe('RemoveNullsAndDuplicatesComponent', () => {
  let component: RemoveNullsAndDuplicatesComponent;
  let fixture: ComponentFixture<RemoveNullsAndDuplicatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveNullsAndDuplicatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveNullsAndDuplicatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
