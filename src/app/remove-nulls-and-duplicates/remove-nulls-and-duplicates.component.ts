import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FileuploadService } from '../Services/fileupload.service';

@Component({
  selector: 'app-remove-nulls-and-duplicates',
  templateUrl: './remove-nulls-and-duplicates.component.html',
  styleUrls: ['./remove-nulls-and-duplicates.component.scss']
})
export class RemoveNullsAndDuplicatesComponent implements OnInit {
  variables1: any;
  filteredList1: any;
  index: any;
  selectedfiles: any = [];
  selectedFile: any = '';
  fileNames: any;
  selectedData: any
  isSinglecolumn: any;
  constructor(public dialogRef: MatDialogRef<RemoveNullsAndDuplicatesComponent>, private fileuploadService: FileuploadService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  title: string = '';
  selectionItems: string[] = ['Single Column', 'Entire DataFrame'];
  selectionItems1: string[] = ['Entire DataFrame']
  selectedValue: any;
  selectedColumn: any;
  columnNames: any;
  newColsToAddDropdown: any;

  ngOnInit(): void {
    this.title = this.data[0];
    if (this.title == "Remove Duplicates") {
      this.selectionItems = this.selectionItems1;
    }
    this.columnNames = [...this.data[1], ...this.fileuploadService.savedFileCols]

    this.fileNames = [...this.data[2], ...this.fileuploadService.saveFileObj];
    for (let i = 0; i < this.fileNames.length; i++) {
      this.selectedfiles.push(this.fileNames[i].name);
    }
    this.newColsToAddDropdown = this.data[2];
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.dialogRef.close([this.selectedValue, this.selectedColumn, this.selectedData, this.title]);
  }
  valuechange() {
    this.fileuploadService.testSavedFile = this.selectedFile.name
    this.index = this.selectedfiles.indexOf(this.selectedFile.name)
    this.fileuploadService.uploadIndex = this.index;

    this.selectedData = this.fileuploadService.data;
    this.variables1 = this.columnNames[this.index];
    this.filteredList1 = this.variables1.slice();
  }

}
