import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeBoatColumnComponent } from './life-boat-column.component';

describe('LifeBoatColumnComponent', () => {
  let component: LifeBoatColumnComponent;
  let fixture: ComponentFixture<LifeBoatColumnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeBoatColumnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeBoatColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
