import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as d3 from 'd3';
import * as d3Shape from 'd3';


@Component({
  selector: 'app-life-boat-column',
  templateUrl: './life-boat-column.component.html',
  styleUrls: ['./life-boat-column.component.scss']
})
export class LifeBoatColumnComponent implements OnInit {
  private datas = [
    { "value": "None", "count": "166443" },
    { "value": "13", "count": "150793" },
    { "value": "C", "count": "62342" },
    { "value": "28", "count": "27647" },
    { "value": "30", "count": "21471" },
  ];
  private svg: any;
  private margin = 50;
  private width = 550 - (this.margin * 2);
  private height = 300 - (this.margin * 2);
  constructor(public dialogRef: MatDialogRef<LifeBoatColumnComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.createSvg();
    this.drawBars(this.datas);
  }

  private createSvg() {
    this.svg = d3.select("figure#bar")
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }
  private drawBars(datas: any[]) {
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(datas.map(d => d.value))
      .padding(0.2);

    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    const y = d3.scaleLinear()
      .domain([0, 200000])
      .range([this.height, 0]);

    this.svg.append("g")
      .call(d3.axisLeft(y));

    this.svg.selectAll("bars")
      .data(datas)
      .enter()
      .append("rect")
      .attr("x", (d: any) => x(d.value))
      .attr("y", (d: any) => y(d.count))
      .attr("width", x.bandwidth())
      .attr("height", (d: any) => this.height - y(d.count))
      .attr("fill", "#f1bbb3");
  }
  onCancel() {
    this.dialogRef.close();
  }
}
