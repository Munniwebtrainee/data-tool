export const messages = {
    serverErrorMsg: 'Something went wrong. Try again later',
    deactivateConfirmMsg: 'You have unsaved changes. Are you sure you want to leave?',
    confirmHeading: 'Confirm!',
    loadingMsg: 'Loading . . . ',
    deleteConfirmHeading: 'Delete Confirmation',
    deleteMsg: 'Are you sure you want to delete',
    noRecordsFoundMsg: 'No records found',
    addedMsg: 'Added Successfully',
    updateMsg: 'Updated Successfully',
    loadingDataMsg: 'Loading data...',
    unCleanedData: 'This column data is not applicable to Mean & Median'

};