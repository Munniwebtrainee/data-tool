import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { AutocleansingComponent } from '../autocleansing/autocleansing.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { LifeBoatColumnComponent } from '../life-boat-column/life-boat-column.component';
import { FileuploadService } from '../Services/fileupload.service';
import { HttpService } from '../Services/http.service';
import { MatTable, MatTableDataSource } from '@angular/material/table';

import * as XLSX from 'xlsx';
type AOA = any[][];
import { MatPaginator } from '@angular/material/paginator';
import { messages } from '../config/messages';
import { DialogService } from '../Services/dialog.service';


@Component({
  selector: 'app-preprocessing',
  templateUrl: './preprocessing.component.html',
  styleUrls: ['./preprocessing.component.scss']
})
export class PreprocessingComponent implements OnInit {
  pcaMethods = ["Keep variance", "Keep accordince"];
  normlMethods = ["Standardization", "Keep accordince"];
  replaceMethods = [
    { name: "mean", value: 'mean' },
    { name: "median", value: 'median' },
    { name: "mode", value: 'mode' },
    { name: "BackwardFill", value: 'BFill' },
    { name: "ForwardFill", value: 'FFill' },
    { name: "specifiedvalue", value: 'specifiedvalue' }
  ];
  method_Names = [
    {
      button: { name: 'REMOVE LOW QUALITY', value: 'lowQuality' },
      formArray: [
        { formControlName: 'maxNominal', formControlType: 'input', label: "Max Nominal ID-ness(%)" },
        { formControlName: 'maxInteger', formControlType: 'input', label: "Max Integer ID-ness(%)" },
        { formControlName: 'maxNominalId', formControlType: 'input', label: "Max Nominal values" },
        { formControlName: 'maxStability', formControlType: 'input', label: "Max Stability(%)" },
        { formControlName: 'maxMissing', formControlType: 'input', label: "Max Missing(%)" }
      ],

    },
    {
      button: { name: 'REMOVE CORRELATED', value: 'removeCorrelated' },
      formArray: [{ formControlName: 'correlatedvalue', formControlType: 'input' },
      ]
    },
    {
      button: { name: 'REPLACE MISSING', value: 'replaceMissing' },
      formArray: [{ formControlName: 'replacedOption', formControlType: 'select' },
      { formControlName: 'replaceValue', formControlType: 'input', label: "Nominal Missings" }
      ]
    },
    {
      button: { name: 'NORMALIZATION', value: 'normalization' },
      formArray: [
        { formControlName: 'normalOption', formControlType: 'select' }]
    },
    {
      button: { name: 'DISCRETIZATION', value: 'discretization' },
      formArray: [{ formControlName: 'discrOption', formControlType: 'select' },
      { formControlName: 'discrValue', formControlType: 'input', label: "Number of bins" }
      ]
    },
    { button: { name: 'DUMMY ENCODING', value: 'dummyencoding' } },
    {
      button: { name: 'PCA', value: 'pca' },
      formArray: [{ formControlName: 'pcaOption', formControlType: 'select' },
      { formControlName: 'pcaValue', formControlType: 'input' },
      ]
    },
    {
      button: { name: 'REMOVE DUPLICATES', value: 'removeDups' },
      formArray: [{ formControlName: 'removeDup', formControlType: 'checkbox' },
      ]
    }
  ];
  // methodNames = ["dummyencoding", "removeduplicates", "pca", "normalization", "dicretization", "replacemissing", "removelowquality", "removecorrelated"];
  messages: any = messages;
  tableLength: any;
  firstIndex = 0;
  lastIndex = 99;
  filteredData: any = [];
  show: boolean = false;
  isShown: boolean = false;
  isLoading: boolean = false;
  isCommit: boolean = false;
  isShowPca: boolean = false;
  isShowNormal: boolean = false;
  isShowDiscrmnal: boolean = false;
  isShowReplace: boolean = false;
  isShowRemove: boolean = false;
  isShowcorrelat: boolean = false;
  isShowDuplic: boolean = false;
  isShowDummy: boolean = false;
  lifeColumn: boolean = false;
  tableColumns: boolean = false;
  disableApply: any = [];
  displayLastChange: boolean = false;
  count: any = 0;
  fileData: any;
  fileInfo: any;
  fileName: any;
  activeColumn: any;
  columnNames: any = [];
  selectedColumnHeader: any;
  menuTrigger: any;
  columnData: any = [];
  selectedCellIndex: any;
  selectedMethod: any;
  Form: FormGroup;
  _obj: any;
  _methods: any;
  buttonId: any;
  enabledMethods: any = [];
  isCancel: boolean = false;
  replaceApiRes: any = [];
  enableApiRes: any = [];
  index: any = [];
  file_Data: any = [];
  _columnData: any = [];
  lastUsed: any;
  modeValue: any;
  mode_column: any = [];
  applyDisabled: boolean = false;
  indexValues: any = [];
  activated_column: any;
  appliedIndex: any = [];
  applieddup: any = [];
  applyDupDisabled: boolean = false;
  isChecked: any;
  isDuplicate: boolean = false;
  cancelledData: any;
  selectedOption: any;
  selectedOption1: any;
  selectedOption2: any;
  selectedOption3: any;
  selectedOption4: any;
  selectedOption5: any;
  cleanseDisabled: boolean = true;
  beforeApply: any = [];
  afterApply: any = [];
  isUndo: boolean = false;
  commit: any;
  uncommit: any;
  // dataSource = new MatTableDataSource(this.fileuploadservice.preprocessTableData);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource: any = new MatTableDataSource();
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;

  }
  constructor(public fb: FormBuilder, public dialog: MatDialog, public fileuploadservice: FileuploadService,
    public httpService: HttpService, public dialogService: DialogService) {
    this.Form = this.fb.group({
      methodName: new FormControl(),
      inputName: new FormControl({ value: null, disabled: true }),
      removeDup: false,
      pca: new FormControl(),
      maxNominal: new FormControl(),
      maxInteger: new FormControl(),
      maxNominalId: new FormControl(),
      maxStability: new FormControl(),
      maxMissing: new FormControl(),

      lowQuality: new FormControl()
    });

  }
  ngOnInit() {

    // this.commitCleanse();
    // this.isCancel = false;
    // if (this.fileuploadservice.preprocessTableData.length == 0) {
    //   this.dataSource.data = []
    //   this.columnNames = Object.keys(this.fileuploadservice.preprocessTableData[0]);

    // }
    this.tableLength = this.fileuploadservice.preprocessTableData.length;

    if (this.fileuploadservice.preprocessTableData.length > 2) {
      if (this.lastIndex >= this.tableLength) {
        this.lastIndex = this.tableLength - 1
      }
      for (let i = this.firstIndex; this.lastIndex >= i; i++) {

        this.filteredData.push(this.fileuploadservice.preprocessTableData[i])
      }
      this._columnData = Object.keys(this.fileuploadservice.preprocessTableData[0]);

      this.dataSource.data = this.filteredData;
    }
    // this._columnData = Object.keys(this.fileuploadservice.preprocessTableData[0]);
    // this.dataSource.data = this.fileuploadservice.preprocessTableData;

  }
  cleans() {
    this.isShown = true;

  }
  applyCommit() {
    this.afterApply = [];
    console.log(this.fileuploadservice.preprocessTableData, "forward")
    this.file_Data = this.fileuploadservice.preprocessTableData;
    this.count = 0;
    this.isCommit = true;
    // this.columnData = [];
    // this.mode_column = [];
    // this.activeColumn = -1
    if (this.isShowReplace == true) {
      this.appliedIndex.push(this.activated_column);
      this.columnData = [];
      let key: any = [];
      key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
      let vals = key[this.activated_column];
      console.log(this.fileuploadservice.preprocessTableData, "first")
      this.fileuploadservice.preprocessTableData.forEach((element: any) => {
        this.columnData.push({ [vals]: element[vals] });

        if (element[vals] == 'NaN' || element[vals] == undefined || element[vals] == 'null' || element[vals] == 'nan' || element[vals] == NaN || element[vals] == '') {
          console.log(element[vals], "jjjjj")
          if (this.Form.controls.methodName.value == 'specifiedvalue') {
            element[vals] = this.Form.controls.inputName.value;
            this.selectedOption5 = "specifiedvalue";
          }
          else if (this.Form.controls.methodName.value == 'mode') {
            this.mode_column = [];
            let key: any = [];
            key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
            let vals = key[this.activated_column];
            this.fileuploadservice.preprocessTableData.forEach((element: any) => {
              if (element[vals] !== undefined) {
                this.mode_column.push(element[vals]);

              }
            })

            // let i = 0
            // while (i < this.mode_column.length) {
            //   if (this.mode_column[i] == this.mode_column[i + 1]) {
            //     this.modeValue = this.mode_column[i]
            //     i += 1
            //   } else {
            //     i += 1

            //   }

            // }
            // element[vals] = this.modeValue;

            // if (this.mode_column.length == 0)
            //   return null;
            var modeMap: any = {};
            var maxEl = this.mode_column[0], maxCount = 1;
            for (var i = 0; i < this.mode_column.length; i++) {
              var el = this.mode_column[i];
              if (modeMap[el] == null)
                modeMap[el] = 1;
              else
                modeMap[el]++;
              if (modeMap[el] > maxCount) {
                maxEl = el;
                maxCount = modeMap[el];
              }
            }
            element[vals] = maxEl;
            if (maxEl == undefined) {
              element[vals] = "undefined"
            }
            return maxEl
          }
          else if (this.Form.controls.methodName.value == "mean") {
            this.mode_column = [];
            let key: any = [];
            key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
            let vals = key[this.activated_column];
            this.fileuploadservice.preprocessTableData.forEach((element: any) => {
              this.mode_column.push(element[vals]);
            })
            this.mode_column = this.mode_column.filter((el: any) => {
              return el !== undefined || NaN || null
            })
            var sum = 0;
            for (let i = 0; i < this.mode_column.length; i++) {

              sum += Number(this.mode_column[i]);


            }
            element[vals] = sum / this.mode_column.length
          }
          else if (this.Form.controls.methodName.value == "median") {
            this.mode_column = [];
            let key: any = [];
            key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
            let vals = key[this.activated_column];
            this.fileuploadservice.preprocessTableData.forEach((element: any) => {
              this.mode_column.push(element[vals]);
            })
            this.mode_column = this.mode_column.filter((el: any) => {
              return el !== undefined || NaN || null
            })

            this.mode_column = this.mode_column.sort((a: any, b: any) => a - b);
            var length = Math.floor(this.mode_column.length / 2);
            if (this.mode_column.length % 2) {
              element[vals] = this.mode_column[length];
              // element[vals] = (this.mode_column[length / 2 - 1] + this.mode_column[length / 2]) / 2
            }
            else {
              element[vals] = (Number(this.mode_column[length - 1]) + Number(this.mode_column[length])) / 2;
              // element[vals] = this.mode_column[(length - 1) / 2];
            }

          }
          else if (this.Form.controls.methodName.value == "BackwardFill") {
            this.selectedOption3 = "BackwardFill";

            this.mode_column = [];
            let key: any = [];
            key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
            let vals = key[this.activated_column];
            this.fileuploadservice.preprocessTableData.forEach((element: any) => {
              this.mode_column.push(element[vals]);
            });
            this.index = this.mode_column.map((v: any, index: any) => v === undefined ? this.mode_column[index + 1] : v)
            for (let i = 0; i < this.index.length; i++) {
              this.fileuploadservice.preprocessTableData[i][this.selectedColumnHeader] = this.index[i];
            }
          }
          else if (this.Form.controls.methodName.value == "ForwardFill") {
            this.selectedOption4 = "ForwardFill";
            console.log(this.fileuploadservice.preprocessTableData, "forwardsssssss")

            this.mode_column = [];
            let key: any = [];
            key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
            let vals = key[this.activated_column];
            this.fileuploadservice.preprocessTableData.forEach((element: any) => {
              this.mode_column.push(element[vals]);
            });
            this.index = this.mode_column.map((v: any, index: any) => v === undefined ? this.mode_column[index - 1] : v)
            for (let i = 0; i < this.index.length; i++) {
              this.fileuploadservice.preprocessTableData[i][this.selectedColumnHeader] = this.index[i];
            }
            console.log(this.fileuploadservice.preprocessTableData, "forward")
          }
        }

      })
    }

    else if (this.isShowDuplic == true) {
      this.applieddup.push(this.activated_column)
      // this.isChecked = true;
      this.isDuplicate = true;
      const arr = this.fileuploadservice.preprocessTableData

      if (this.Form.controls.removeDup.value == true) {
        function getUniqueListBy(arr: any, key: any) {
          return [...new Map(arr.map((item: any) => [item[key], item])).values()]
        }
        let arr1 = getUniqueListBy(arr, this.selectedColumnHeader);
        this.fileuploadservice.preprocessTableData = [];
        arr1.forEach((el: any) => {
          this.fileuploadservice.preprocessTableData.push(el);

        })
        console.log(arr1, "arr111111")

      }
      else {
        const arrs: any = [];
        arr.reduce((acc, curr) => {
          if (curr[this.selectedColumnHeader] !== undefined) {
            if (acc.indexOf(curr[this.selectedColumnHeader]) === -1) {
              acc.push(curr[this.selectedColumnHeader]);
              arrs.push(curr);
            }
          }
          else {
            acc.push(curr[this.selectedColumnHeader]);
            arrs.push(curr);
          }
          return acc;
        }, [])
        this.fileuploadservice.preprocessTableData = arrs;
        console.log(arrs)

        this.isDuplicate = true;
      }


      this._columnData = Object.keys(this.fileuploadservice.preprocessTableData[0]);
      this.dataSource.data = this.fileuploadservice.preprocessTableData;
    }
    this.isShowReplace = !this.isShowReplace
    // let key: any = [];
    // key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
    // let vals = key[this.activeColumn];
    // this.fileuploadservice.preprocessTableData.forEach((element: any) => {
    //   this.afterApply.push({ [vals]: element[vals] });
    // })
    // console.log(this.afterApply, "afterapply")
    this.activeColumn = -1;
    // if(this.mode_column.includes(NaN) && this.isCommit) {
    //   this.dialogService.openDialog(messages.unCleanedData);

    // }
  }
  pagesize(event: any) {
    let page = event.pageSize;
    this.firstIndex = this.lastIndex + 1;
    this.lastIndex = page + this.firstIndex - 1;
    if (this.lastIndex >= this.tableLength) {
      this.lastIndex = this.tableLength - 1
    }
    if (this.lastIndex <= this.tableLength) {
      for (let i = this.firstIndex; this.lastIndex >= i; i++) {
        this.filteredData.push(this.fileuploadservice.preprocessTableData[i])
      }
    }
    this.dataSource.data = this.filteredData;
  }
  selectMethod(name: any) {

    this.lastUsed = name;
    if (this.isShowReplace == true) {
      // this.selectedMethod = document.getElementById('replace')?.innerText
      this.selectedMethod = name;
    }
  }
  // toggleDisplay(i: any, val: any) {

  //   this.buttonId = i;
  //   this.isShowPca = !this.isShowPca;
  // }

  toggleDisplay1() {
    this.isShowNormal = !this.isShowNormal;
    this.isShowPca = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
    this.isShowDummy = false;
  }
  toggleDisplay2() {
    this.isShowDuplic = !this.isShowDuplic;
    this.isShowNormal = false;
    this.isShowPca = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
    this.isShowDummy = false;
  }
  toggleDisplay3() {
    this.isShowDiscrmnal = !this.isShowDiscrmnal;
    this.isShowNormal = false;
    this.isShowPca = false;
    this.isShowDuplic = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
    this.isShowDummy = false;
  }
  toggleDisplay4() {
    this.isShowRemove = !this.isShowRemove;
    this.isShowNormal = false;
    this.isShowPca = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
    this.isShowDummy = false;
  }
  toggleDisplay5() {
    this.isShowcorrelat = !this.isShowcorrelat;
    this.isShowNormal = false;
    this.isShowPca = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowReplace = false;
    this.isShowDummy = false;
  }
  toggleDisplay6() {
    this.isShowReplace = !this.isShowReplace;
    this.isShowNormal = false;
    this.isShowPca = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowDummy = false;
  }
  toggleDisplay7() {
    this.isShowDummy = !this.isShowDummy;
    this.isShowNormal = false;
    this.isShowPca = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
  }
  setValue() {
    this.disableApply = [];
    this.disableApply.push(this.Form.controls.methodName.value);
    if (this.Form.controls.methodName.value == 'specifiedvalue') {
      this.Form.controls.inputName.enable();
    }
    else {
      this.Form.controls.inputName.disable();
    }
  }

  autoCleansingDialog() {
    const dialogRef = this.dialog.open(AutocleansingComponent, {
      width: '80%',
      height: '80%'
    });
    dialogRef.afterClosed().subscribe((result: any) => {
    });
  }
  lifeBoatColumnDialog() {
    this.lifeColumn = !this.lifeColumn;
    const dialogRef = this.dialog.open(LifeBoatColumnComponent, {
      width: '37%',
      height: '25%'
    });
    dialogRef.afterClosed().subscribe((result: any) => {
    });
  }
  selectColumn(val: any, i: any) {
    // if (this.isUndo) {
    //   this.fileuploadservice.preprocessTableData = [];
    //   this.fileuploadservice.preprocessTableData = this.fileuploadservice.uncomitData;
    // }
    console.log(this.fileuploadservice.preprocessTableData, "forwardyyyy")

    this.beforeApply = [];
    this.isCancel = false;
    // this.cleanseDisabled = false;
    this.activeColumn = i;
    this.mode_column = [];
    let key: any = [];
    let checkColumn: any = [];
    key = Object.keys(this.fileuploadservice.preprocessTableData[0]);
    let vals = key[this.activeColumn];
    this.fileuploadservice.preprocessTableData.forEach((element: any) => {
      if (element[vals] == undefined) {
        this.mode_column.push(element[vals]);
      }
      else {
        this.mode_column.push(Number(element[vals]));

      }
      checkColumn.push(element[vals]);
      // this.beforeApply.push(element[vals]);
      this.beforeApply.push({ [vals]: element[vals] });
      // this.beforeApply = this.fileuploadservice.preprocessTableData.map((item: any, i: any) => Object.assign({}, item, this.beforeApply[i]));

    })
    console.log(this.mode_column, "mmmmmmmmmmm")
    console.log(this.beforeApply, "beforeApply")

    if (this.mode_column.includes(NaN)) {
      this.dialogService.openDialog(messages.unCleanedData);

      this.selectedOption = "mean";
      this.selectedOption1 = "median";
      // this.selectedOption2 = "mode";

    }
    else {
      this.selectedOption = "";
      this.selectedOption1 = "";
      this.selectedOption2 = "";
    }
    if (this.indexValues.length == 0 && this.disableApply.length == 0) {
      this.applyDisabled = false;
    }
    else if (this.indexValues.includes(this.activeColumn)) {
      this.disableApply = [];
      // if (this.disableApply.length == 0 && this.appliedIndex.includes(this.activeColumn) && !checkColumn.includes(undefined)) {
      if (!this.mode_column.includes(undefined)) {
        this.applyDisabled = true;
        // this.isShowReplace = !this.isShowReplace;

      }
      else {
        this.applyDisabled = false

      }
    }
    else {
      this.selectedOption3 = '';
      this.selectedOption4 = '';
      this.selectedOption5 = '';
      this.applyDisabled = false;
    }

    if (this.applieddup.includes(this.activeColumn)) {
      this.applyDupDisabled = true;
      this.isChecked = true;
    }
    else {
      this.isChecked = false;
      this.applyDupDisabled = false
    }
    this.tableColumns = !this.tableColumns;
    this.indexValues.push(this.activeColumn);
    this.indexValues = [...new Set(this.indexValues)]

    // this.activeColumn = i;
    this.activated_column = i
    this.columnData = [];
    // this.selectedColumnHeader = this.fileuploadservice.preprocessTableData[0][this.activeColumn];
    this.selectedColumnHeader = val;
    this.count = 1;

    // this.file_Data = this.fileuploadservice.uncomitData;


    // console.log(this.file_Data, "fileeeeeeeeeeeeee")
    // this.isShowReplace = !this.isShowReplace;

    this.enableMethods();

  }

  onFileSelect(event: any) {
    this.isLoading = true;

    this.fileuploadservice.filename = event.target.files[0].name;
    this.fileData = event.target.files[0];
    const target: DataTransfer = <DataTransfer>(event.target);
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary', cellDates: true });
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      /* save data */
      this.fileuploadservice.preprocessTableData = (XLSX.utils.sheet_to_json(ws, { raw: false, dateNF: 'yyyy-mm-dd hh:mm:ss' }));//changed date format
      this.fileuploadservice.uncomitData = (XLSX.utils.sheet_to_json(ws, { raw: false, dateNF: 'yyyy-mm-dd hh:mm:ss' }));
      this.isLoading = false;
      this.tableLength = this.fileuploadservice.preprocessTableData.length;
      this.columnNames = Object.keys(this.fileuploadservice.preprocessTableData[0]);

      for (let i = this.firstIndex; this.lastIndex >= i; i++) {

        this.filteredData.push(this.fileuploadservice.preprocessTableData[i])
      }
      // this._columnData = Object.keys(this.filteredData[0]);
      this.dataSource.data = this.filteredData;
      this.fileuploadservice.cancelledData = this.fileuploadservice.preprocessTableData;
      this._columnData = Object.keys(this.fileuploadservice.preprocessTableData[0]);
      this.dataSource.data = this.fileuploadservice.preprocessTableData;
      this.fileuploadservice.rows = this.fileuploadservice.preprocessTableData.length;
      this.fileuploadservice.columns = this._columnData.length;
    };
    reader.readAsBinaryString(target.files[0]);
    this.fileName = this.fileuploadservice.filename;
    var extension = this.fileName.split('?')[0].split('.').pop();
    if (extension != 'xls' || extension != 'XLSX') {
      this.fileInfo = '';
    }

  }



  // converTableToJson() {
  //   let obj: any = {};
  //   for (let index = 0; index < this.fileuploadservice.preprocessTableData[0].length; index++) {
  //     const element = this.fileuploadservice.preprocessTableData[0][index];
  //     obj[element] = {};
  //     for (let i = 0; i < this.fileuploadservice.preprocessTableData.length; i++) {
  //       if (this.fileuploadservice.preprocessTableData[i] != undefined && i > 0) {
  //         let el = this.fileuploadservice.preprocessTableData[i][index];
  //         
  //         obj[element][i - 1] = el == undefined || el == "" ? "NaN" : el;
  //       }
  //     }
  //   }
  //   this._obj = JSON.stringify(obj);
  // }
  // methodsData() {
  //   if (this.isShowReplace == true && this.isShowDuplic == false) {
  //     if (this.isShowReplace == true && this.Form.controls.methodName.value == 'mode' && this.isShowDuplic == false) {

  //       this._methods = {
  //         method: this.selectedMethod, type: this.Form.controls.methodName.value,
  //         text: this.Form.controls.inputName.value, column: this.selectedColumnHeader
  //       };
  //       delete this._methods.text
  //       this._methods = JSON.stringify(this._methods);
  //     }
  //     else {
  //       this._methods = {
  //         method: this.selectedMethod, type: this.Form.controls.methodName.value,
  //         text: this.Form.controls.inputName.value, column: this.selectedColumnHeader
  //       };
  //       this._methods = JSON.stringify(this._methods);
  //     }

  //   }

  //   else if (this.isShowReplace == false && this.isShowDuplic == true) {
  //     this._methods = {
  //       method: this.selectedMethod, check: this.Form.controls.removeDup.value.toString(), column: this.selectedColumnHeader
  //     };
  //     this._methods = JSON.stringify(this._methods);
  //   }
  //   else {
  //     this._methods = {}
  //   }
  //   const body = new FormData();
  //   // if (this.Form.controls.methodName.value == 'specifiedvalue') {
  //   body.append('datafile', JSON.stringify(this.fileuploadservice.preprocessTableData).replace(/\:null/gi, "\:'NaN'"));
  //   // }
  //   // else {
  //   //   body.append('datafile', JSON.stringify(this.fileuploadservice.preprocessTableData).replace(/\:null/gi, "\:'NaN'"));

  //   // }
  //   body.append('text1', this._methods);
  //   this.httpService.post('https://datamigrationtool.azurewebsites.net/preprocessing', body).subscribe((res: any) => {
  //     this.fileuploadservice.preprocessTableData = res
  //     this._columnData = Object.keys(res[0]);
  //     this.dataSource.data = res
  //     if (res) {
  //       this.isShowReplace = false;
  //       this.isShowDuplic = false;


  //     }

  //   })
  //   // this.replaceApiRes.forEach((el: any) => {
  //   //   for (const value of Object.values(el)) {
  //   //     if (this.isShowDuplic == true) {
  //   //       for (let m = 0; m < this.fileuploadservice.preprocessTableData[0].length; m++) {
  //   //         if (m > 0) {
  //   //           this.fileuploadservice.preprocessTableData[m].splice(this.fileuploadservice.preprocessTableData[0].length, 2, value);
  //   //         }
  //   //       }
  //   //     }
  //   //   }
  //   // });
  //   this._methods = {}
  // }
  commitCleanse() {
    this.isCancel = true
    this.displayLastChange = true;
    this.dataSource.data = [];
    // this.file_Data = [];
    this._columnData = Object.keys(this.fileuploadservice.preprocessTableData[0]);
    this.dataSource.data = this.fileuploadservice.preprocessTableData;
    // this.file_Data = this.fileuploadservice.preprocessTableData;
    // console.log(this.file_Data)
    this.fileuploadservice.lastChange = this.lastUsed;

  }
  unCommit() {
    // if (this.isCancel) {
    //   this.commitCleanse()
    // }
    this.selectedOption3 = '';
    this.selectedOption4 = '';
    this.selectedOption5 = '';
    console.log(this.file_Data);
    this.isUndo = true;
    this.columnData = [];
    this.activeColumn = -1;
    this.applieddup = [];
    this.appliedIndex = [];
    if (this.isCancel) {

      // const mergedArr = this.fileuploadservice.preprocessTableData.map((item: any, i: any) => Object.assign({}, item, this.afterApply[i]));

      // console.log(mergedArr, "ggggggggoooooo")

      console.log(this.fileuploadservice.preprocessTableData, "kkkkkkkkk")
      this._columnData = Object.keys(this.fileuploadservice.preprocessTableData[0]);

      this.dataSource.data = this.fileuploadservice.preprocessTableData;


      //   // this._columnData = Object.keys(this.fileuploadservice.preprocessTableData[0]);
      //   // this.dataSource.data = this.fileuploadservice.preprocessTableData;
      //   console.log(this.dataSource.data, "after commit")
      //   this.isShown = false;
      //   this.enabledMethods = [];
      //   this.isCommit = false;
      //   this.isCancel = false;
      //   this.applyDisabled = false;
      //   this.indexValues = [];
      //   this.disableApply = [];
      //   this.appliedIndex = [];
    }
    else {

      // this.columnData = [];
      // let key: any = [];
      let mergedArr: any;
      if (this.Form.controls.removeDup.value == true || this.Form.controls.removeDup.value == false && this.isDuplicate == true) {
        mergedArr = this.file_Data;
      }
      else {
        mergedArr = this.fileuploadservice.preprocessTableData.map((item: any, i: any) => Object.assign({}, item, this.beforeApply[i]));
      }
      console.log(mergedArr, "gggggggg")
      this.fileuploadservice.preprocessTableData = mergedArr;


      //   console.log(this.fileuploadservice.uncomitData, "kkkkkkkkkbbbbbbbbb", this.beforeApply)
      this._columnData = Object.keys(mergedArr[0]);

      this.dataSource.data = mergedArr;
      this.isDuplicate = false;
      //   this.isShown = false;
      //   this.applyDisabled = false;
      //   this.indexValues = [];
      //   this.disableApply = [];
      //   this.appliedIndex = [];

    }
  }

  enableMethods() {
    this.enableApiRes = [];
    this.enabledMethods = [];
    this.isLoading = true;
    this._methods = {
      column: this.selectedColumnHeader
    };
    this._methods = JSON.stringify(this._methods);
    const body = new FormData();
    this.enableApiRes = [];
    this.enabledMethods = [];
    body.append('datafile1', JSON.stringify(this.fileuploadservice.preprocessTableData).replace(/\:null/gi, "\:'NaN'"));
    body.append('text2', this._methods);
    this.httpService.post('https://datamigrationtool.azurewebsites.net/enablingmethods', body).subscribe((res: any) => {
      if (res) {
        this.isLoading = false;
      }
      this.enableApiRes = res
      this.enableApiRes.forEach((element: any) => {
        this.enabledMethods.push(element.methodname);
        this.disabled(element.methodname);
      });
    },
      err => {
        this.isLoading = false;
        this.dialogService.openDialog(messages.serverErrorMsg);
      })


  }
  disabled(val: any) {
    if (this.enabledMethods.includes(val)) {
      // return false
      return true
    }
    // return true
    return false
  }
  applyFilter(event: Event) {
    const filterData = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterData.trim().toLowerCase()
  }
}
