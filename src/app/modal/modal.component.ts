import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
export interface DialogData {
  message: string;
  subMessage?: string;
  dialogTitle?: string;
  buttons?: { label: string, handler: () => any, active?: boolean }[];
  displayLoader?: boolean;
  htmlMsg?: any;
  iconType?: any;
  redirectTo?: '';

}
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private router: Router) { }

  ngOnInit() {

  }


  onOKClick(): void {
    this.dialogRef.close();
    if (this.data.redirectTo) {
      this.router.navigate([this.data.redirectTo]);
    }
  }

}
