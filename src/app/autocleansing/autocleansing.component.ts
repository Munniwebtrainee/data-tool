import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';


@Component({
  selector: 'app-autocleansing',
  templateUrl: './autocleansing.component.html',
  styleUrls: ['./autocleansing.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class AutocleansingComponent implements OnInit {
  tableHeaders = [
    { header: "Passenger class", type: 'Category' },
    { header: "Name", type: 'Category' },
    { header: 'Gender', type: 'Category' },
    { header: "Age", type: 'Number' },
    { header: "No.of siblings", type: 'Number' },
    { header: "Ticket number", type: 'Number' },
    { header: "Passenger fare", type: 'Number' },
    { header: "Cabin", type: 'Category' },
    { header: "Life Boat", type: 'Number' },
    { header: "Survived", type: 'Category' }
  ]
  tableData = [
    { class: 'first', name: 'alen', gender: 'female', age: 20, siblings: 2, ticktNo: 667521, fare: 450, cabin: 'S2', boat: 2, survive: 'yes' },
    { class: 'first', name: 'alex', gender: 'male', age: 22, siblings: 1, ticktNo: 667421, fare: 420, cabin: 'S1', boat: 1, survive: 'no' },
    { class: 'second', name: 'joey', gender: 'female', age: 21, siblings: 2, ticktNo: 767521, fare: 650, cabin: 'S2', boat: 12, survive: 'yes' },
    { class: 'first', name: 'brad', gender: 'male', age: 20, siblings: 3, ticktNo: 663321, fare: 350, cabin: 'B2', boat: 8, survive: 'yes' },
    { class: 'third', name: 'brokey', gender: 'female', age: 25, siblings: 2, ticktNo: 3367521, fare: 350, cabin: 'D1', boat: 6, survive: 'yes' },

  ];

  normlMethods = ["Keep Original", "Change all to numbers", "change all to categorical"];
  constructor(public dialogRef: MatDialogRef<AutocleansingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,) { }

  ngOnInit(): void {
  }
  onCancel() {
    this.dialogRef.close();
  }
}
