import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocleansingComponent } from './autocleansing.component';

describe('AutocleansingComponent', () => {
  let component: AutocleansingComponent;
  let fixture: ComponentFixture<AutocleansingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutocleansingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocleansingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
