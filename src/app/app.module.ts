import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from './material/material.module';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ResultsComponent } from './results/results.component';
import { SelectColumnsComponent } from './fileupload/select-columns/select-columns.component';
import { RemoveNullsAndDuplicatesComponent } from './remove-nulls-and-duplicates/remove-nulls-and-duplicates.component';
import { HelpDialogComponent } from './fileupload/help-dialog/help-dialog.component';
import { ModalComponent } from './modal/modal.component';
import { SearchPipe } from './search.pipe';
import { JoinfilesComponent } from './joinfiles/joinfiles.component';
import { SavefileComponent } from './savefile/savefile.component';
import { PreprocessingComponent } from './preprocessing/preprocessing.component';
import { AutocleansingComponent } from './autocleansing/autocleansing.component';
import { LifeBoatColumnComponent } from './life-boat-column/life-boat-column.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

// import { LoaderComponent } from './loader/loader.component';
@NgModule({
  declarations: [
    AppComponent,
    FileuploadComponent,
    NavbarComponent,
    LoginComponent,
    ToolbarComponent,
    ResultsComponent,
    SelectColumnsComponent,
    RemoveNullsAndDuplicatesComponent,
    HelpDialogComponent,
    ModalComponent,
    SearchPipe,
    JoinfilesComponent,
    SavefileComponent,
    PreprocessingComponent,
    AutocleansingComponent,
    LifeBoatColumnComponent,
    // LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule, HttpClientModule, Ng2SearchPipeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
