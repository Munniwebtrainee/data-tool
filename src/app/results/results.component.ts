import { Component, ElementRef, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FileuploadService } from '../Services/fileupload.service';
import * as XLSX from 'xlsx';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { SavefileComponent } from '../savefile/savefile.component';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { DialogService } from '../Services/dialog.service';
import { messages } from '../config/messages';
import { MatPaginator } from '@angular/material/paginator';
import { HttpService } from '../Services/http.service';

interface Student {
  id: Number;
  name: String;
  email: String;
  gender: String;
}
@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})

export class ResultsComponent implements OnInit, OnChanges {
  pcaMethods = ["Keep variance", "Keep accordince"];

  normlMethods = ["Standardization", "Keep accordince"];
  replaceMethods = [
    { name: "mean", value: 'mean' },
    { name: "median", value: 'median' },
    { name: "mode", value: 'mode' },
    { name: "BackwardFill", value: 'BFill' },
    { name: "ForwardFill", value: 'FFill' },
    { name: "specifiedvalue", value: 'specifiedvalue' }
  ];
  method_Names = [
    {
      button: { name: 'REMOVE LOW QUALITY', value: 'lowQuality' },
      formArray: [
        { formControlName: 'maxNominal', formControlType: 'input', label: "Max Nominal ID-ness(%)" },
        { formControlName: 'maxInteger', formControlType: 'input', label: "Max Integer ID-ness(%)" },
        { formControlName: 'maxNominalId', formControlType: 'input', label: "Max Nominal values" },
        { formControlName: 'maxStability', formControlType: 'input', label: "Max Stability(%)" },
        { formControlName: 'maxMissing', formControlType: 'input', label: "Max Missing(%)" }
      ],

    },
    {
      button: { name: 'REMOVE CORRELATED', value: 'removeCorrelated' },
      formArray: [{ formControlName: 'correlatedvalue', formControlType: 'input' },
      ]
    },
    {
      button: { name: 'REPLACE MISSING', value: 'replaceMissing' },
      formArray: [{ formControlName: 'replacedOption', formControlType: 'select' },
      { formControlName: 'replaceValue', formControlType: 'input', label: "Nominal Missings" }
      ]
    },
    {
      button: { name: 'NORMALIZATION', value: 'normalization' },
      formArray: [
        { formControlName: 'normalOption', formControlType: 'select' }]
    },
    {
      button: { name: 'DISCRETIZATION', value: 'discretization' },
      formArray: [{ formControlName: 'discrOption', formControlType: 'select' },
      { formControlName: 'discrValue', formControlType: 'input', label: "Number of bins" }
      ]
    },
    { button: { name: 'DUMMY ENCODING', value: 'dummyencoding' } },
    {
      button: { name: 'PCA', value: 'pca' },
      formArray: [{ formControlName: 'pcaOption', formControlType: 'select' },
      { formControlName: 'pcaValue', formControlType: 'input' },
      ]
    },
    {
      button: { name: 'REMOVE DUPLICATES', value: 'removeDups' },
      formArray: [{ formControlName: 'removeDup', formControlType: 'checkbox' },
      ]
    }
  ];

  // 
  messages: any = messages;

  tableData: any;
  _tableData: any;
  result_data: boolean = true;
  colName1: any;
  objVal: any = '';
  arrOfValues: any;
  isLoading: any;
  selectedFormat: string = '';
  issaved = false;
  isShown: boolean = false;
  isShowNormal: boolean = false;
  isShowDuplic = false;
  isShowDiscrmnal = false;
  isShowRemove = false;
  isShowcorrelat = false;
  isShowReplace = false;
  beforeApply: any = [];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource = new MatTableDataSource();
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;
  activeColumn: any;
  mode_column: any = [];
  selectedOption = '';
  selectedOption1 = '';
  selectedOption2 = '';
  indexValues: any = [];
  disableApply: any = [];
  applyDisabled: boolean = false;
  selectedOption3 = '';
  selectedOption4 = '';
  selectedOption5 = '';
  applyDupDisabled: boolean = false;
  isChecked: boolean = false;
  tableColumns: boolean = false;
  activated_column: any;
  columnData: any = [];
  selectedColumnHeader: any;
  enableApiRes: any = [];
  enabledMethods: any = [];
  _methods: any = {};
  applieddup: any = [];
  index: any;
  appliedIndex: any = [];
  isDuplicate: boolean = false;
  Form: FormGroup;
  lastUsed: any;
  selectedMethod: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;

  }
  TABLE!: ElementRef;
  title = 'Excel';
  isdeleted = false;
  constructor(public fb: FormBuilder, public fileuploadservice: FileuploadService, public dialog: MatDialog, private router: Router, private dialogService: DialogService, private httpService: HttpService) {
    this.Form = this.fb.group({
      methodName: new FormControl(),
      inputName: new FormControl({ value: null, disabled: true }),
      removeDup: false,
      pca: new FormControl(),
      maxNominal: new FormControl(),
      maxInteger: new FormControl(),
      maxNominalId: new FormControl(),
      maxStability: new FormControl(),
      maxMissing: new FormControl(),

      lowQuality: new FormControl()
    });
  }
  fileFormats = ['XLSX', 'CSV'];
  deletedColIndex: any;
  deletedColumn: any = [];
  columnNames: any = [];
  searchText: any;
  undoData: any = [];
  filteredData: any = [];
  pageSize = 0;
  firstIndex: any = 0;
  lastIndex: any = 99;
  beforeDelete: any = [];
  columnDeleted: boolean = false;
  file_Data: any = [];
  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }
  ngOnInit(): void {
    this.columnNames = Object.keys(this.fileuploadservice.data[0]);
    this.fileuploadservice.columns = this.columnNames.length;
    this.fileuploadservice.rows = this.fileuploadservice.tableLength;

    if (this.lastIndex >= this.fileuploadservice.tableLength) {
      this.lastIndex = this.fileuploadservice.tableLength - 1
    }
    for (let i = this.firstIndex; this.lastIndex >= i; i++) {

      this.filteredData.push(this.fileuploadservice.data[i])
    }

    this.dataSource.data = this.filteredData;
    console.log(this.dataSource.data, this.fileuploadservice.columns, this.fileuploadservice.rows, this.fileuploadservice.tableLength, "data")
  }


  downloadFile() {
    this.isdeleted = true;
    if (this.selectedFormat == 'XLSX' || this.selectedFormat == 'CSV') {
      var body = new FormData();
      var filenum: any;
      if (this.fileuploadservice.savedFiles.includes(this.fileuploadservice.testSavedFile)) {
        filenum = "savedfile"
      }
      else {
        filenum = this.fileuploadservice.deleteColFileIndex
      }
      body.append('filenum', filenum);
      this.httpService
        .post('http://127.0.0.1:5000/DownloadFile ', body)
        .subscribe(
          (res: any) => {
            if (res) {
              console.log(res, "down")

              const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(res);
              const wb: XLSX.WorkBook = XLSX.utils.book_new();
              XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

              if (this.selectedFormat == 'XLSX') {
                XLSX.writeFile(wb, 'table_data.xlsx');
              }
              else {
                XLSX.writeFile(wb, 'table_data.csv');
              }

            }
          })
    }
  }
  pagesize(event: any) {

    this.firstIndex = event.pageIndex * event.pageSize + 50;
    this.lastIndex = this.firstIndex + event.pageSize - 1;

    const formData = new FormData();
    formData.append('startindex', this.firstIndex);
    formData.append('endindex', this.lastIndex);
    formData.append('filenum', this.fileuploadservice.deleteColFileIndex)
    this.httpService
      .post('http://127.0.0.1:5000/DisplayDatabase ', formData)
      .subscribe(
        (res: any) => {
          if (res) {
            for (let i = 0; i < res.length; i++) {
              this.fileuploadservice.data.push(res[i]);

            }
            if (this.lastIndex >= this.fileuploadservice.tableLength) {
              this.lastIndex = this.fileuploadservice.tableLength - 1
            }
            if (this.lastIndex <= this.fileuploadservice.tableLength) {
              for (let i = this.firstIndex; this.lastIndex >= i; i++) {
                this.filteredData.push(this.fileuploadservice.data[i])
              }
            }
            this.dataSource.data = this.filteredData;
          }
          console.log(res, "tableDisplay")
        })


  }
  onColumnselect(i: any) {
    let index = i;
    this.deleteSelectedCoumn(i);
  }
  deleteSelectedCoumn(i: any) {
    this.fileuploadservice.beforeColDelete = [];
    this.beforeDelete = this.fileuploadservice.data;
    this.fileuploadservice.beforeColDelete.push(Object.keys(this.beforeDelete[0]))
    const options = {
      dialogTitle: messages.deleteConfirmHeading,
      message: messages.deleteMsg
    }
    const dialogRef = this.dialogService.openConfirmDialog(options);
    dialogRef.afterClosed().subscribe(closeType => {
      if (closeType && closeType === 'Yes') {
        this.isLoading = true;
        let index = i;
        var colName = this.columnNames[index]
        const body = new FormData();
        body.append('selectedcol', colName);
        body.append('selectedtype', "Delete")
        body.append('filenum', this.fileuploadservice.deleteColFileIndex)
        this.httpService
          .post('http://127.0.0.1:5000/DeleteColmDatabase', body)
          .subscribe((data: any) => {
            console.log(data, "deletedata")
            if (data) {
              this.fileuploadservice.data = [];
              for (let i = 0; i < data.length; i++) {
                this.fileuploadservice.data.push(data[i])
              }
              this.isLoading = false;
              this.columnNames = Object.keys(this.fileuploadservice.data[0])
              this.dataSource.data = this.fileuploadservice.data
              console.log(this.dataSource.data, "jjjjjj")

              this.fileuploadservice.columns = this.columnNames.length;
              this.fileuploadservice.rows = this.fileuploadservice.tableLength;
            }
            else {
              this.isLoading = false;
              this.dialogService.openDialog(messages.serverErrorMsg);
            }


          })
      }
    })
  }
  undoChanges() {
    this.isLoading = true;

    if (this.isShown == true) {
      const body = new FormData();
      body.append('filenum', this.fileuploadservice.deleteColFileIndex)
      this.httpService
        .post('http://127.0.0.1:5000/undopreprocessing', body)
        .subscribe((data: any) => {
          if (data) {
            this.fileuploadservice.data = [];
            for (let i = 0; i < data.length; i++) {
              this.fileuploadservice.data.push(data[i])
            }
            this.fileuploadservice.tableLength = data.length

            this.isLoading = false;
            this.columnNames = Object.keys(this.fileuploadservice.data[0])
            this.dataSource.data = this.fileuploadservice.data
            this.fileuploadservice.columns = this.columnNames.length;
            this.fileuploadservice.rows = this.fileuploadservice.tableLength;

          }
        }, err => {
          this.isLoading = false;
          this.dialogService.openDialog(messages.serverErrorMsg)
        })
    }
    else {
      this.fileuploadservice.beforeColDelete = [];

      const body = new FormData();
      body.append('selectedtype', "Undo")
      body.append('filenum', this.fileuploadservice.deleteColFileIndex)

      this.httpService
        .post('http://127.0.0.1:5000/DeleteColmDatabase', body)
        .subscribe((data: any) => {
          if (data) {
            this.fileuploadservice.data = [];
            for (let i = 0; i < data.length; i++) {
              this.fileuploadservice.data.push(data[i])
            }
            // this.fileuploadservice.tableLength = data.length

            this.isLoading = false;
            this.columnNames = Object.keys(this.fileuploadservice.data[0])
            this.dataSource.data = this.fileuploadservice.data
            this.fileuploadservice.columns = this.columnNames.length;
            this.fileuploadservice.rows = this.fileuploadservice.tableLength;
            this.fileuploadservice.beforeColDelete.push(Object.keys(this.fileuploadservice.data[0]))

          }
        }, err => {
          this.isLoading = false;
          this.dialogService.openDialog(messages.serverErrorMsg)
        })

    }

  }

  saveFile() {
    this.isdeleted = true;
    const dialogRef = this.dialog.open(SavefileComponent, {
      width: '30%',
      height: '40vh',
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.isdeleted = false;

      if (result) {
        let file = result;
        this.fileuploadservice.savedFiles.push(file);
        this.fileuploadservice.saveFileObj.push({ name: result, data: this.fileuploadservice.data });
        this.fileuploadservice.savedFileCols.push(this.columnNames)
        console.log(this.fileuploadservice.savedFileCols);

      }
    })

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  cleans() {
    this.isShown = !this.isShown;
    if (this.isShown == true) {
      this.isdeleted = true;

    }
    else {
      this.isdeleted = false;
    }

  }
  toggleDisplay1() {
    this.isShowNormal = !this.isShowNormal;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
  }
  toggleDisplay2() {
    this.isShowDuplic = !this.isShowDuplic;
    this.isShowNormal = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
  }
  toggleDisplay3() {
    this.isShowDiscrmnal = !this.isShowDiscrmnal;
    this.isShowNormal = false;
    this.isShowDuplic = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
  }
  toggleDisplay4() {
    this.isShowRemove = !this.isShowRemove;
    this.isShowNormal = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowcorrelat = false;
    this.isShowReplace = false;
  }
  toggleDisplay5() {
    this.isShowcorrelat = !this.isShowcorrelat;
    this.isShowNormal = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowReplace = false;
  }
  toggleDisplay6() {
    this.isShowReplace = !this.isShowReplace;
    this.isShowNormal = false;
    this.isShowDuplic = false;
    this.isShowDiscrmnal = false;
    this.isShowRemove = false;
    this.isShowcorrelat = false;
  }
  selectColumn(val: any, i: any) {
    this.beforeApply = [];
    this.activeColumn = i;
    this.mode_column = [];
    let key: any = [];
    let checkColumn: any = [];
    key = Object.keys(this.fileuploadservice.data[0]);
    let vals = key[this.activeColumn];
    this.fileuploadservice.data.forEach((element: any) => {
      if (element[vals] == null) {
        this.mode_column.push(element[vals]);
      }
      else {
        this.mode_column.push(Number(element[vals]));

      }
      checkColumn.push(element[vals]);
      this.beforeApply.push({ [vals]: element[vals] });

    })


    if (this.mode_column.includes(NaN)) {
      this.dialogService.openDialog(messages.unCleanedData);

      this.selectedOption = "mean";
      this.selectedOption1 = "median";

    }
    else {
      this.selectedOption = "";
      this.selectedOption1 = "";
      this.selectedOption2 = "";
    }
    if (this.indexValues.length == 0 && this.disableApply.length == 0) {
      this.applyDisabled = false;
    }
    else if (this.indexValues.includes(this.activeColumn)) {
      this.disableApply = [];
      console.log(this.mode_column, "modeeeeeee")
      if (!this.mode_column.includes(null)) {
        this.applyDisabled = true;

      }
      else {
        this.applyDisabled = false

      }
    }
    else {
      this.selectedOption3 = '';
      this.selectedOption4 = '';
      this.selectedOption5 = '';
      this.applyDisabled = false;
    }

    if (this.applieddup.includes(this.activeColumn)) {
      this.applyDupDisabled = true;
      this.isChecked = true;
    }
    else {
      this.isChecked = false;
      this.applyDupDisabled = false
    }
    this.tableColumns = !this.tableColumns;
    this.indexValues.push(this.activeColumn);
    this.indexValues = [...new Set(this.indexValues)]

    this.activated_column = i
    this.columnData = [];
    this.selectedColumnHeader = val;

    this.enableMethods();

  }
  enableMethods() {
    this.enableApiRes = [];
    this.enabledMethods = [];
    this.isLoading = true;

    const body = new FormData();
    this.enableApiRes = [];
    this.enabledMethods = [];
    body.append('text1', JSON.stringify(this.selectedColumnHeader));
    body.append('filenum', this.fileuploadservice.deleteColFileIndex)
    this.httpService.post('http://127.0.0.1:5000/enablingmethods', body).subscribe((res: any) => {
      if (res) {
        this.isLoading = false;
      }
      this.enableApiRes = res
      this.enableApiRes.forEach((element: any) => {
        this.enabledMethods.push(element);
        this.disabled(element);
      });
    },
      err => {
        this.isLoading = false;
        this.dialogService.openDialog(messages.serverErrorMsg);
      })


  }
  disabled(val: any) {
    if (this.enabledMethods.includes(val)) {
      return false
    }
    return true
  }
  applyCommit() {
    this.isLoading = true;
    var body = new FormData();
    var method: any;
    if (this.isShowReplace == true) {
      method = "replacemissing";
      if (this.Form.controls.methodName.value == 'specifiedvalue') {
        var text: any = {
          method: method,
          type: this.Form.controls.methodName.value,
          column: this.selectedColumnHeader,
          text: this.Form.controls.inputName.value
        }

      }

      else {
        if (this.Form.controls.methodName.value == 'BackwardFill') {
          this.selectedOption3 = "BackwardFill";
        }
        if (this.Form.controls.methodName.value == 'ForwardFill') {
          this.selectedOption3 = "ForwardFill";

        }
        var text: any = {
          method: method,
          type: this.Form.controls.methodName.value,
          column: this.selectedColumnHeader
        }
      }
      body.append('filenum', this.fileuploadservice.deleteColFileIndex);
      body.append('text1', JSON.stringify(text))
      this.httpService.post('http://127.0.0.1:5000/preprocessing', body).subscribe((res: any) => {
        if (res) {
          this.filteredData = [];
          for (let i = 0; i < res.length; i++) {
            this.filteredData.push(res[i])
          }
          this.isLoading = false;
          this.fileuploadservice.data = res;
          this.columnNames = Object.keys(this.fileuploadservice.data[0]);
          this.dataSource.data = this.filteredData;
          this.fileuploadservice.columns = this.columnNames.length;
          this.fileuploadservice.rows = this.fileuploadservice.tableLength;
          console.log(res, "rrrrrrr")
        }
      }, err => {
        this.isLoading = false;
        this.dialogService.openDialog(messages.serverErrorMsg);
      })
    }
    else {
      const body = new FormData();
      body.append('MethodName', 'Remove Duplicates');
      body.append('Duplicateselect', 'Single Column');
      body.append('Select SingleCol', this.selectedColumnHeader);
      var filenum = this.fileuploadservice.deleteColFileIndex
      body.append('filenum', filenum);
      body.append('check', this.Form.controls.removeDup.value)
      this.httpService
        .post('http://127.0.0.1:5000/RemovecountDatabase', body)
        .subscribe((data: any) => {
          if (data >= 0) {
            this.fileuploadservice.tableLength = this.fileuploadservice.uploadedfilesData[this.fileuploadservice.deleteColFileIndex - 1]
            console.log(this.fileuploadservice.tableLength, "table", this.fileuploadservice.uploadedfilesData)
            if (this.fileuploadservice.tableLength != data) {

              const options = {
                dialogTitle: messages.deleteConfirmHeading,
                message: messages.deleteMsg + " " + (this.fileuploadservice.tableLength - data) + "" + "rows"
              }

              const dialogRef = this.dialogService.openConfirmDialog(options);
              dialogRef.afterClosed().subscribe(closeType => {
                if (closeType && closeType === 'Yes') {
                  if (filenum == 1) {
                    this.fileuploadservice.uploadedfilesData.splice(0, 1, data);
                  }
                  else if (filenum == 2) {
                    this.fileuploadservice.uploadedfilesData.splice(1, 1, data);

                  }
                  else if (filenum == 3) {
                    this.fileuploadservice.uploadedfilesData.splice(2, 1, data);

                  }
                  else {
                    this.fileuploadservice.updatedrowcount.splice(0, data);
                  }
                  this.fileuploadservice.tableLength = data;
                  method = "removeduplicates"
                  var text: any = {
                    method: method,
                    check: JSON.stringify(this.Form.controls.removeDup.value),
                    column: this.selectedColumnHeader
                  }
                  // body.append('filenum', this.fileuploadservice.deleteColFileIndex);
                  body.append('text1', JSON.stringify(text))
                  this.httpService.post('http://127.0.0.1:5000/preprocessing', body).subscribe((res: any) => {
                    if (res) {
                      this.filteredData = [];
                      for (let i = 0; i < res.length; i++) {
                        this.filteredData.push(res[i])
                      }
                      // if (method == "removeduplicates") {
                      // this.fileuploadservice.tableLength = res.length

                      // }
                      this.isLoading = false;
                      this.fileuploadservice.data = res;
                      this.columnNames = Object.keys(this.fileuploadservice.data[0]);
                      this.dataSource.data = this.filteredData;
                      this.fileuploadservice.columns = this.columnNames.length;
                      this.fileuploadservice.rows = this.fileuploadservice.tableLength;
                      console.log(res, "rrrrrrr")
                    }
                  }, err => {
                    this.isLoading = false;
                    this.dialogService.openDialog(messages.serverErrorMsg);
                  })

                }
                else {
                  var params = new FormData();
                  params.append('filenum', filenum);
                  this.fileuploadservice.tableLength = this.fileuploadservice.uploadedfilesData[this.fileuploadservice.deleteColFileIndex - 1]
                  this.httpService
                    .post("http://127.0.0.1:5000/GetselectData", params)
                    .subscribe((data: any) => {
                      this.filteredData = [];
                      if (data) {
                        for (let i = 0; i < data.length; i++) {
                          this.filteredData.push(data[i])
                        }
                        this.isLoading = false;

                      }

                    }, err => {
                      this.isLoading = false;
                      this.dialogService.openDialog(messages.serverErrorMsg);

                    })
                }

              })
            }
          }
        })
    }


    this.activeColumn = -1;
    this.isShowReplace = !this.isShowReplace;


  }
  selectMethod(name: any) {

    this.lastUsed = name;
    if (this.isShowReplace == true) {
      this.selectedMethod = name;
    }
  }
  setValue() {
    this.disableApply = [];
    this.disableApply.push(this.Form.controls.methodName.value);
    if (this.Form.controls.methodName.value == 'specifiedvalue') {
      this.Form.controls.inputName.enable();
    }
    else {
      this.Form.controls.inputName.disable();
    }
  }
}
