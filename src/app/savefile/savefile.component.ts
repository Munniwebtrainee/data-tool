import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { messages } from '../config/messages';
import { DialogService } from '../Services/dialog.service';
import { FileuploadService } from '../Services/fileupload.service';
import { HttpService } from '../Services/http.service';
import * as XLSX from 'xlsx';
// import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-savefile',
  templateUrl: './savefile.component.html',
  styleUrls: ['./savefile.component.scss']
})
export class SavefileComponent implements OnInit {
  selectTypes = ['.xlxs', '.csv'];
  selectType: any;
  filename: any
  sfile: any;
  blobToFile: any;
  isLoading = false;
  tableToJsonObjForAddTrans: any;
  saveFileNames: any = [];
  saveFileData: any = [];
  @ViewChild('TABLE', { static: false })
  TABLE!: ElementRef;
  constructor(public dialogRef: MatDialogRef<SavefileComponent>, private fileuploadService: FileuploadService, private httpService: HttpService, private dialogService: DialogService) { }

  ngOnInit(): void {
  }
  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.isLoading = true;
    let obj: any = {};
    for (let index = 0; index < this.fileuploadService.data[0].length; index++) {
      const element = this.fileuploadService.data[0][index];
      obj[element] = {};
      for (let i = 0; i < this.fileuploadService.data.length; i++) {
        if (this.fileuploadService.data[i] != undefined && i > 0) {
          const el = this.fileuploadService.data[i][index];
          // obj[element][i-1] = el;
          obj[element][i - 1] = el == null || el == "" ? undefined : el;
        }
      }
    }
    this.tableToJsonObjForAddTrans = JSON.stringify(obj);

    let arr = [];

    arr.push(JSON.parse(this.tableToJsonObjForAddTrans))



    let filen = this.filename + ".xlsx";



    this.dialogRef.close(filen);


  }

}
