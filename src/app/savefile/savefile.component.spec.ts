import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavefileComponent } from './savefile.component';

describe('SavefileComponent', () => {
  let component: SavefileComponent;
  let fixture: ComponentFixture<SavefileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavefileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavefileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
