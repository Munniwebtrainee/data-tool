import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinfilesComponent } from './joinfiles.component';

describe('JoinfilesComponent', () => {
  let component: JoinfilesComponent;
  let fixture: ComponentFixture<JoinfilesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoinfilesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
