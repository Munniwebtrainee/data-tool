import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { messages } from '../config/messages';
import { DialogService } from '../Services/dialog.service';
import { FileuploadService } from '../Services/fileupload.service';
import { HttpService } from '../Services/http.service';

@Component({
  selector: 'app-joinfiles',
  templateUrl: './joinfiles.component.html',
  styleUrls: ['./joinfiles.component.scss']
})
export class JoinfilesComponent implements OnInit {
  fileName: any;
  selectedFile: any = '';
  selectedFile1: any = '';

  methodKeys = ['inner', 'outer', 'left', 'right'];
  joincount = [2, 3];
  variables1: any;
  variables2: any;
  variables3: any;

  filteredList1: any;
  filteredList2: any;
  filteredList3: any;
  isLoading = false;

  index: any;
  selectedfiles: any = [];
  column_name1 = '';
  column_name2 = '';
  column_name3 = '';
  selectcount: any = '';
  selectedFile2: any;
  methodKey = '';
  columnNames: any;
  uploadIndex: any;
  uploadIndex1: any;
  uploadIndex2: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<JoinfilesComponent>, private fileuploadService: FileuploadService, private httpService: HttpService, private dialogService: DialogService) { }

  ngOnInit(): void {
    this.fileName = [...this.data[0], ...this.fileuploadService.savedFiles];

    this.columnNames = [...this.data[1], ...this.fileuploadService.savedFileCols]
    // for (let index = 0; index < this.fileuploadService.saveFileNames.length; index++) {
    //   let file = [];
    //   let obj = { name: this.fileuploadService.saveFileNames[index] };
    //   this.fileName.push(obj);
    // }

    // this.fileName = [...this.fileName,]

    // for (let i = 0; i < this.fileuploadService.savedFilesList.length; i++) {
    //   this.fileNames.push(this.fileuploadService.savedFilesList[i])


    // }
    // this.fileNames.push(this.fileuploadService.savedFilesList[0]);
    for (let i = 0; i < this.fileName.length; i++) {
      this.selectedfiles.push(this.fileName[i].name);
    }
  }
  onCancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.isLoading = true;
    const body = new FormData();
    body.append('countfiles', this.selectcount);
    if (this.selectcount == 2) {
      body.append('file1', "Table" + this.uploadIndex);
      body.append('file2', "Table" + this.uploadIndex1);
      body.append('key1', this.column_name1);
      body.append('key2', this.column_name2);
    }
    if (this.selectcount == 3) {
      body.append('file1', "Table" + this.uploadIndex);
      body.append('file2', "Table" + this.uploadIndex1);
      body.append('file3', "Table" + this.uploadIndex2);
      body.append('key1', this.column_name1);
      body.append('key2', this.column_name2);
      body.append('key3', this.column_name3);
    }
    body.append('method', this.methodKey);
    this.fileuploadService.deleteColFileIndex = "joinfile";

    this.httpService.post("http://127.0.0.1:5000/JoinTableDatabase", body).subscribe((res) => {
      if (res) {
        this.isLoading = false;
        this.dialogRef.close(res);
      }
      else {
        this.isLoading = false;
      }

    }, err => {
      this.isLoading = false;

      this.dialogService.openDialog(messages.serverErrorMsg);
    })
  }
  valuechange1() {
    this.fileuploadService.testSavedFile = this.selectedFile.name

    this.index = this.selectedfiles.indexOf(this.selectedFile.name)
    this.uploadIndex = this.index + 1;

    this.variables1 = this.columnNames[this.index];
    // this.variables2 = this.data[1][this.index];
    this.filteredList1 = this.variables1.slice();
    // this.filteredList2 = this.variables2.slice();
    // }

    // }


  }
  valuechange2() {
    this.fileuploadService.testSavedFile = this.selectedFile1.name

    this.index = this.selectedfiles.indexOf(this.selectedFile1.name)
    this.uploadIndex1 = this.index + 1;

    // this.variables1 = this.data[1][this.index];
    this.variables2 = this.columnNames[this.index];
    // this.filteredList1 = this.variables1.slice();
    this.filteredList2 = this.variables2.slice();
    // }

    // }


  }
  valuechange3() {
    this.fileuploadService.testSavedFile = this.selectedFile2.name

    this.index = this.selectedfiles.indexOf(this.selectedFile2.name)
    this.uploadIndex2 = this.index + 1;

    // this.variables1 = this.data[1][this.index];
    this.variables3 = this.columnNames[this.index];
    // this.filteredList1 = this.variables1.slice();
    this.filteredList3 = this.variables3.slice();
    // }

    // }


  }
  ngOnDestroy() {
    this.fileName = [];
  }
}
