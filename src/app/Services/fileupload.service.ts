import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import { HttpService } from './http.service';
type AOA = any[][];

@Injectable({
  providedIn: 'root'
})
export class FileuploadService {
  preprocessTableData: AOA = [[1, 2], [3, 4]];
  saveFileNames: any = [];
  saveFileData: any = [];
  fileInfo = '';
  inputFilePath = '';
  data: AOA = [[1, 2], [3, 4]];
  // data1: AOA = [[1, 2], [3, 4]];
  uncomitData: any = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  file: any;
  selectedTransName = '';
  selectedCols = '';
  selectedText = '';
  uploaded_file = '';
  userText: any;
  // tableDataJson: any = [];
  multiFiles: any;
  count = 0;
  file1: any;
  file2: any;
  methodName = '';
  key1 = '';
  key2 = '';
  key3 = '';
  fileData1: any;
  fileData2: any;
  fileData3: any;
  sampleData: any = [];
  uploadedfilesData: any = [];
  uploadIndex: any;
  saveFileObj: any = [];
  savedFileCols: any = [];
  savedFiles: any = [];
  transFile: any = [];
  rows: any;
  columns: any;
  filename: any;
  filenum: any;
  lastChange: any = '';
  cancelledData: any;
  responsetable: any = [];
  beforeColDelete: any = [];
  tableLength: any;
  isJoined = true;
  deleteColFileIndex: any;
  testSavedFile: any;
  fileIndex: any;
  updatedrowcount: any = [];
  constructor(
    private httpService: HttpService
  ) { }

  getTabledata(): any {
    const body = new FormData();

    body.append("transradiotxt1", this.selectedTransName);
    body.append("colmchecktxt1", this.selectedCols);
    body.append("text", this.selectedText);
    body.append("inputchecktxt1", this.userText);
    var filenum;
    if (this.savedFiles.includes(this.testSavedFile)) {
      filenum = "savedfile"
    }
    else {
      filenum = this.uploadIndex + 1
    }
    body.append("filenum", filenum)
    this.deleteColFileIndex = this.uploadIndex + 1;


    return this.httpService.post("http://127.0.0.1:5000/transformationDatabase", body);
  }


}
