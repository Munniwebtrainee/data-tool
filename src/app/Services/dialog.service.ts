import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs/internal/Observable';
import { messages } from '../config/messages';
import { ModalComponent } from '../modal/modal.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  dialogRef: any;
  username: any;
  modalOptions: any = {
    default: { width: '350px', maxHeight: '450px' },
    confirm: { width: '450px', maxHeight: '250px' }
  };
  constructor(public dialog: MatDialog) { }

  // display message to the user
  openDialogWithLoader(message: string) {
    if (message) {
      this.dialogRef = this.dialog.open(ModalComponent, {
        ...this.modalOptions.default,
        disableClose: true,
        data: {
          message,
          displayLoader: true
        }
      });
    }
  }

  // close dialog
  closeDialog() {
    this.dialogRef.close();
  }

  // change message in dialog
  changeMsgInDialog(message: string) {
    if (message && this.dialogRef && this.dialogRef.componentInstance) {
      this.dialogRef.componentInstance.data.message = message;
      this.dialogRef.componentInstance.data.displayLoader = false;
    }
  }



  // Open Normal dialog with message
  openDialog(message: any, iconType?: string) {
    if (message) {
      const dialogOptions: any = {
        ...this.modalOptions.default,
        data: {}
      };
      if (typeof message === 'string') {
        dialogOptions.data.message = message;
      } else { // object
        dialogOptions.data = { ...message };
      }
      if (iconType) {
        dialogOptions.data.iconType = iconType;
      }
      this.dialog.open(ModalComponent, dialogOptions);
    }
  }

  // confirmation alert buttons handler
  showCanDeactivateConfirm(): Observable<boolean> | boolean | Promise<boolean> {
    return new Promise((resolve, reject) => {
      const dialogRef = this.dialog.open(ModalComponent, {
        width: '450px',
        disableClose: true,
        data: {
          dialogTitle: messages.confirmHeading,
          message: messages.deactivateConfirmMsg,
          buttons: [
            {
              label: 'Stay',
              handler: () => dialogRef.close('Stay')
            },
            {
              label: 'Leave',
              active: true,
              handler: () => {
                dialogRef.close('Leave');
              }
            }
          ]
        }
      });

      dialogRef.afterClosed().subscribe((closeType: any) => {
        if (closeType && closeType === 'Stay') { // Cancel click
          resolve(false);
        } else { // Ok click
          resolve(true);
        }
      }, () => {
        reject(false);
      });
    });
  }

  // Open confirm dialog
  openConfirmDialog(options: Object) {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '350px',
      data: {
        ...options,
        buttons: [
          {
            label: 'No',
            handler: () => dialogRef.close('No')

          },
          {
            label: 'Yes',
            active: true,
            handler: () => {
              dialogRef.close('Yes');
            }
          }
        ]
      }
    });
    return dialogRef;
  }


}
