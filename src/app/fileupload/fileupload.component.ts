import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FileuploadService } from '../Services/fileupload.service';
import { HttpService } from '../Services/http.service';
import * as XLSX from 'xlsx';
import { SelectColumnsComponent } from './select-columns/select-columns.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { RemoveNullsAndDuplicatesComponent } from '../remove-nulls-and-duplicates/remove-nulls-and-duplicates.component';
import { HelpDialogComponent } from './help-dialog/help-dialog.component';
import { DialogService } from '../Services/dialog.service';
import { messages } from '../config/messages';
import { findIndex } from 'rxjs/operators';
import { JoinfilesComponent } from '../joinfiles/joinfiles.component';


type AOA = any[][];
@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.scss'],
})
export class FileuploadComponent implements OnInit {
  fileInfo: any;
  isShown = false;
  isDisabled = false;
  isoutputDisabled = false;
  showTransformations = false;
  transformationNames = [];
  transformationsArr: any = [];
  selectedTransformationsArr: any = [];
  selectedFiles: any = [];
  fileData: any;
  columnNames: any[] = [];
  selectedTransName: string = '';
  isEnabled: boolean = false;
  appliedTransformationNamesArr: any = [];
  show: boolean = false;
  designTab: boolean = true;
  resultTab: boolean = false;
  preTab: boolean = false;
  isLoading = false;
  is_disabled: any = false;
  tableData: any;
  appliedColumns: any = [];
  appliedNewColumns: any = [];
  appliedInputs: any = [];
  isTransformationsEnabled = false;
  runDisabled = true;
  isClicked = false;
  committedTransformations: any = [];
  joinfilesExpand: any;
  removeNullsExpand: any;
  multipleFiles: File[] = [];
  uploadedFiles: any = [];
  notPresentInData: any = [];
  removeDuplicates = ['Remove Nulls', 'Remove Duplicates'];
  showCommittedTransNames: boolean = false;
  selectedType: any;
  selectedCol: any;
  _selectedColumnToAppend: any;
  clicked_itemName: any;
  filesData: any = [];
  multiFileCols: any = [];
  methods = ['left', 'right', 'inner', 'outer'];
  uploadedFilesColHeaders: any = [];
  resTransNames: any = [];
  successCommitedarray: any = [];
  columnData: any = [];
  failedCommitedarray: any = [];
  pindex: any;
  findex: any;
  transfiles: any = [];
  failedData: any = [];
  preLength: any;
  curLength: any;
  oldData: any;
  inputdata: any;
  removeNullData: any = [];
  newColNames: any = [];
  constructor(
    private router: Router,
    public fileuploadservice: FileuploadService,
    public httpService: HttpService,
    public dialog: MatDialog,
    private dialogService: DialogService
  ) {
  }

  ngOnInit(): void {
    this.isShown = true;
    this.isoutputDisabled = true;
    this.isDisabled = true;

  }
  designtab() {
    this.designTab = true;
    this.resultTab = false;
    this.preTab = false;

  }
  resulttab() {
    this.designTab = false;
    this.resultTab = true;
    this.preTab = false;

  }

  async onFileSelect(event: any) {
    this.isLoading = true;

    this.fileInfo = event.target.files[0].name;
    this.fileData = event.target.files[0];
    if (this.fileuploadservice.responsetable.includes(this.fileInfo)) {
      alert("file already exists")
      this.isLoading = false;

    }
    else {
      if (this.fileuploadservice.responsetable.length > 2) {
        alert('more than 3 files are not allowed');
        this.isLoading = false;
      }
      else {
        this.fileuploadservice.data = [];
        this.fileuploadservice.responsetable.push(this.fileInfo)

        var filenum = this.fileuploadservice.responsetable.indexOf(this.fileInfo) + 1
        console.log(this.fileuploadservice.responsetable, "rrrrrrr")

        const formData = new FormData();
        formData.append('file', this.fileData);
        formData.append('filenum', filenum);
        this.fileuploadservice.deleteColFileIndex = filenum;
        this.httpService
          .post('http://127.0.0.1:5000/uploadDatabase ', formData)
          .subscribe(
            (res: any) => {
              if (res) {
                for (let i = 0; i < res.length; i++) {
                  this.fileuploadservice.data.push(res[i]);

                }


                const target: DataTransfer = <DataTransfer>(event.target);
                if (target.files.length !== 1) throw new Error('Cannot use multiple files');
                const reader: FileReader = new FileReader();
                reader.onload = async (e: any) => {
                  /* read workbook */
                  const bstr: string = (e.target.result);
                  const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary', cellDates: true, cellText: false });

                  /* grab first sheet */
                  const wsname: string = wb.SheetNames[0];
                  const ws: XLSX.WorkSheet = wb.Sheets[wsname];

                  /* save data */
                  // this.fileuploadservice.data = (XLSX.utils.sheet_to_json(ws, { defval: undefined, raw: false, dateNF: 'yyyy-mm-dd hh:mm:ss' }));
                  this.inputdata = (XLSX.utils.sheet_to_json(ws, { defval: undefined }));
                  this.fileuploadservice.tableLength = this.inputdata.length;

                  this.fileuploadservice.uploadedfilesData.push(this.inputdata.length);
                  console.log(this.fileuploadservice.uploadedfilesData, "filesdata")
                  this.columnNames = Object.keys(this.fileuploadservice.data[0]);
                  if (this.multiFileCols.length < 3) {
                    this.multiFileCols.push(this.columnNames);
                  }
                  this.isLoading = false;

                  this.resultTab = true;
                  this.designTab = false;
                };

                reader.readAsBinaryString(target.files[0]);

                var fileName = this.fileInfo;
                var extension = fileName.split('?')[0].split('.').pop();
                if (extension != 'xls' || extension != 'XLSX') {
                  this.fileInfo = '';
                }

                this.filesData.push(fileName);

                if (this.multipleFiles.length < 3) {
                  this.uploadedFiles = [];
                  this.uploadedFiles.push(this.fileData);
                  this.multipleFiles.push(this.fileData);

                }
                this.isShown = true;
                this.isClicked = true;
                this.isDisabled = false;
                if (this.fileInfo) {
                  this.isClicked = true;
                }
                this.fileuploadservice.multiFiles = this.uploadedFiles;
                this.fileuploadservice.file = this.uploadedFiles[0];
              }
              else {
                this.isLoading = false;

              }
              console.log(res, this.fileuploadservice.data, "table")
            }, (err) => {
              this.isLoading = false;
              this.dialogService.openDialog(messages.serverErrorMsg);
            })
      }
    }
  }
  arrowExpansion() {
    this.isDisabled = !this.isDisabled;
  }
  outputExpansion() {
    this.isoutputDisabled = !this.isoutputDisabled;

  }
  onShowTransformations() {
    this.showTransformations = !this.showTransformations;
    this.getTransformationsApi();
  }
  getTransformationsApi() {
    this.isLoading = true;
    this.httpService
      .get('https://datamigrationtool.azurewebsites.net/Translist')
      .subscribe((data: any) => {
        this.transformationNames = data.transformation_types;
        this.transformationsArr = this.transformationNames;
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
        this.dialogService.openDialog(messages.serverErrorMsg);
      });
  }
  selectColumnsDialog() {
    this.newColumnNames();
    this.transfiles = this.multipleFiles;
    const dialogRef = this.dialog.open(SelectColumnsComponent, {
      width: '50%',
      data: [this.multiFileCols, this.selectedTransName, this.transfiles]

    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.isEnabled = true;
        this.appliedTransformationNamesArr.push(result[0]);
        this.selectedTransformationsArr = [];
        this.appliedColumns.push(result[1].length ? result[1] : "'null'");
        this.appliedNewColumns.push(result[2].length ? result[2] : "'null'");
        this.appliedInputs.push(result[3].length ? result[3] : "'a'");
        this.selectedFiles.push(result[4].length ? result[4] : "'null'");
        this.runDisabled = false;
      }

      else {
        this.runDisabled = true;
      }
      this.fileuploadservice.file = this.selectedFiles;
      this.fileuploadservice.selectedTransName = this.appliedTransformationNamesArr;
      this.fileuploadservice.selectedCols = this.appliedColumns;
      this.fileuploadservice.selectedText = this.appliedNewColumns;
      this.fileuploadservice.userText = this.appliedInputs;

    });
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      copyArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      this.selectedTransName = this.selectedTransformationsArr[0];
      this.isTransformationsEnabled = true;
      this.selectColumnsDialog();
    }
  }
  deleteTransformation(index: any) {
    this.selectedTransformationsArr.splice(index, 1);
  }
  deleteAppliedTransformation(index: any) {
    let indexPosition = index;
    this.appliedTransformationNamesArr.splice(index, 1);
    this.appliedColumns.splice(index, 1);
    this.appliedNewColumns.splice(index, 1);
    this.appliedInputs.splice(index, 1);

  }

  delete(index: any) {
    this.appliedTransformationNamesArr.splice(index, 1);
  }
  addTransName() {
    this.is_disabled = true;
    if (this.appliedTransformationNamesArr.length > 0) {
      this.isTransformationsEnabled = false;
    }
  }
  appendNewColumn() {
    this.fileuploadservice.transFile = [];
    this.isLoading = true;
    this._selectedColumnToAppend = this.appliedNewColumns;

    this.fileuploadservice.getTabledata().subscribe(
      (res: any) => {
        if (res) {

          this.resTransNames = Object.keys(res[0]);
          this.notPresentInData = [];
          this.failedData = [];
          this.columnData = [];
          this.appliedNewColumns.forEach((val: any) => {
            console.log(val, "vala")
            if (this.resTransNames.includes(val)) {
              this.notPresentInData.push(val);

            }

          });

        }
        this.appliedNewColumns.forEach((element: any) => {
          this.columnData.push(element);
        });

        this.failedData = this.columnData.filter((x: any) => !this.notPresentInData.includes(x));

        for (let i = 0; i < this.notPresentInData.length; i++) {
          this.pindex = this.notPresentInData.indexOf(this.notPresentInData[i]);
          this.successCommitedarray.push(this.appliedTransformationNamesArr[this.pindex]);

        }

        if (this.columnData.length != this.notPresentInData.length) {
          this.dialogService.openDialog("Some transformations are failed check in failed transformations ");
          for (let i = 0; i < this.failedData.length; i++) {
            this.findex = this.columnData.indexOf(this.failedData[i]);
            this.failedCommitedarray.push(this.appliedTransformationNamesArr[this.findex]);
          }
        }
        this.tableData = res;
        this.addNewColumn();
        this.isTransformationsEnabled = false;
        this.isLoading = false;

      },

      (error: any) => {
        this.dialogService.openDialog(messages.serverErrorMsg);
        this.isLoading = false;

      });

    if (this._selectedColumnToAppend.length > 0) {
      for (let index = 0; index < this._selectedColumnToAppend.length; index++) {
        this.multiFileCols[this.fileuploadservice.uploadIndex].push(this._selectedColumnToAppend[index]);
        this.uploadedFilesColHeaders.push(this._selectedColumnToAppend[index][0]);
      }
    }
  }

  addNewColumn() {

    this.isLoading = true;
    this.fileuploadservice.data = this.tableData;
    this.resultTab = true;
    this.designTab = false;
    this.isLoading = false;
    for (let index = 0; index < this.appliedTransformationNamesArr.length; index++) {
      const element = this.appliedTransformationNamesArr[index];
      this.committedTransformations.push(element);
    }
    this.appliedTransformationNamesArr = [];
    this.appliedColumns = [];
    this.appliedNewColumns = [];
    this.appliedInputs = [];

    this.showCommittedTransNames = true;
    this.runDisabled = true;

  }
  removeNullsDialog(item: string) {


    this.newColumnNames();
    this.removeNullData = [];
    this.preLength = this.fileuploadservice.data.length;
    this.oldData = this.fileuploadservice.data;
    if (this.multipleFiles.length > 0) {

      let clickedItem = item;
      this.clicked_itemName = item;
      const dialogRef = this.dialog.open(RemoveNullsAndDuplicatesComponent, {
        width: '50%',
        height: '50vh',
        data: [clickedItem, this.multiFileCols, this.multipleFiles]
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          this.isLoading = true;
          var title = result[3];
          this.selectedType = result[0];
          this.selectedCol = result[1];
          const body = new FormData();
          body.append('MethodName', title);
          body.append('Duplicateselect', this.selectedType);
          var filenum: any;
          if (this.fileuploadservice.savedFiles.includes(this.fileuploadservice.testSavedFile)) {
            filenum = "savedfile"
          }
          else {
            filenum = this.fileuploadservice.uploadIndex + 1
          }
          body.append('filenum', filenum);
          this.fileuploadservice.deleteColFileIndex = this.fileuploadservice.uploadIndex + 1;
          var filenumIndex = [];

          if (this.selectedCol != undefined) {
            body.append('Select SingleCol', this.selectedCol);
          }
          var API: any;
          if (clickedItem == 'Remove Nulls') {
            API = 'http://127.0.0.1:5000/RemovenullDatabase'
          }
          else {
            API = 'http://127.0.0.1:5000/RemoveDupDatabase'
          }

          this.httpService
            .post('http://127.0.0.1:5000/RemovecountDatabase', body)
            .subscribe((data: any) => {
              if (data >= 0) {


                console.log(this.fileuploadservice.updatedrowcount, "rowcount")
                this.fileuploadservice.tableLength = this.fileuploadservice.uploadedfilesData[this.fileuploadservice.uploadIndex]
                if (this.fileuploadservice.tableLength != data) {

                  const options = {
                    dialogTitle: messages.deleteConfirmHeading,
                    message: messages.deleteMsg + " " + (this.fileuploadservice.tableLength - data) + "" + "rows"
                  }

                  const dialogRef = this.dialogService.openConfirmDialog(options);
                  dialogRef.afterClosed().subscribe(closeType => {

                    if (closeType && closeType === 'Yes') {
                      if (filenum == 1) {
                        this.fileuploadservice.uploadedfilesData.splice(0, 1, data);
                      }
                      else if (filenum == 2) {
                        this.fileuploadservice.uploadedfilesData.splice(1, 1, data);

                      }
                      else if (filenum == 3) {
                        this.fileuploadservice.uploadedfilesData.splice(2, 1, data);

                      }
                      else {
                        this.fileuploadservice.updatedrowcount.splice(0, data);
                      }
                      this.fileuploadservice.tableLength = data;
                      this.httpService
                        .post(API, body)
                        .subscribe((data: any) => {
                          this.fileuploadservice.data = [];
                          if (data) {
                            for (let i = 0; i < data.length; i++) {
                              this.fileuploadservice.data.push(data[i])
                            }
                            this.isLoading = false;
                            this.resultTab = true;
                            this.designTab = false;
                          }


                        }, err => {
                          this.isLoading = false;
                          this.dialogService.openDialog(messages.serverErrorMsg);
                        })

                    }
                    else {
                      var params = new FormData();
                      params.append('filenum', filenum);
                      this.fileuploadservice.tableLength = this.fileuploadservice.uploadedfilesData[this.fileuploadservice.uploadIndex]
                      this.httpService
                        .post("http://127.0.0.1:5000/GetselectData", params)
                        .subscribe((data: any) => {
                          this.fileuploadservice.data = [];
                          if (data) {
                            for (let i = 0; i < data.length; i++) {
                              this.fileuploadservice.data.push(data[i])
                            }
                            this.isLoading = false;
                            this.resultTab = true;
                            this.designTab = false;
                          }

                        }, err => {
                          this.isLoading = false;
                          this.dialogService.openDialog(messages.serverErrorMsg);

                        })

                    }
                  })
                }
                else {
                  this.isLoading = false;
                  this.resultTab = true;
                  this.designTab = false;
                }
              }

            }, (err) => {
              this.isLoading = false;
              this.dialogService.openDialog(messages.serverErrorMsg);

            });

        }

      });

    }
    else {
      this.dialogService.openDialog("Files Count should be >0")
    }

  }
  newColumnNames() {
    this.newColNames = [];
    this.columnNames = Object.keys(this.fileuploadservice.data[0])
    this.newColNames.push(this.columnNames);
    for (let i = 0; i < this.multiFileCols.length; i++) {
      if (this.multiFileCols[i].toString() == this.fileuploadservice.beforeColDelete.toString()) {
        var index = this.multiFileCols.indexOf(this.multiFileCols[i])
        this.multiFileCols.slice(index)
        this.multiFileCols[index] = this.newColNames[0];
      }
      else {
        this.multiFileCols = this.multiFileCols
      }
    }

  }
  joinfilesDialog() {
    this.newColumnNames();
    this.isLoading = true;
    if (this.multipleFiles.length > 1) {
      const dialogRef = this.dialog.open(JoinfilesComponent, {
        width: '50%',
        height: '65vh',
        data: [this.multipleFiles, this.multiFileCols]
      });
      dialogRef.afterClosed().subscribe(result => {

        if (result) {
          this.httpService.get("http://127.0.0.1:5000/Joincount").subscribe((res) => {
            if (res) {
              this.fileuploadservice.tableLength = res;
              this.fileuploadservice.updatedrowcount.push(res);
              this.fileuploadservice.isJoined = false;
              // this.fileuploadservice.data = [];
              this.resultTab = true;
              this.designTab = false;
              this.fileuploadservice.data = result;
              this.isLoading = false;
            }
          }, err => {
            this.isLoading = false;
            this.dialogService.openDialog(messages.serverErrorMsg)
          })

        }
        else {
          this.isLoading = false;
        }
      })
    }
    else {
      this.isLoading = false
      this.dialogService.openDialog("Files Count should be >1");
    }

  }

  displayFileData(index: any) {
    this.isLoading = true;
    this.fileuploadservice.data = [];
    var body = new FormData();
    body.append('filenum', index + 1);
    this.fileuploadservice.deleteColFileIndex = index + 1;

    this.httpService
      .post('http://127.0.0.1:5000/GetselectData', body)
      .subscribe((data: any) => {
        if (data) {
          this.isLoading = false;

          this.fileuploadservice.data = data;
          this.fileuploadservice.tableLength = this.fileuploadservice.uploadedfilesData[index]
          this.resultTab = true;
          this.designTab = false;

        }
        else {
          this.isLoading = false;
        }
      }, err => {
        this.isLoading = false;
        this.dialogService.openDialog(messages.serverErrorMsg)
      })


  }
  outputFileData(index: any) {
    this.isLoading = true;
    this.fileuploadservice.data = [];
    var body = new FormData();
    body.append('filenum', "savedFile");
    this.fileuploadservice.deleteColFileIndex = "savedFile";

    this.httpService
      .post('http://127.0.0.1:5000/GetselectData', body)
      .subscribe((data: any) => {
        if (data) {
          this.isLoading = false;

          this.fileuploadservice.data = data;
          this.fileuploadservice.tableLength = this.fileuploadservice.updatedrowcount[index]
          this.resultTab = true;
          this.designTab = false;

        }
        else {
          this.isLoading = false;
        }
      }, err => {
        this.isLoading = false;
        this.dialogService.openDialog(messages.serverErrorMsg)
      })





  }
  openHelpDialog() {
    const dialogRef = this.dialog.open(HelpDialogComponent, {
      width: '50%',
      height: '80%'
    });
    dialogRef.afterClosed().subscribe((result) => {
    });
  }
}