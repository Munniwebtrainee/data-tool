import { Component, Inject, OnChanges, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectFilterModule } from 'mat-select-filter';
import { FileuploadService } from 'src/app/Services/fileupload.service';
import { FileuploadComponent } from '../fileupload.component';


export interface PeriodicElement {
  id: any;
  column: string;
}



@Component({
  selector: 'app-select-columns',
  templateUrl: './select-columns.component.html',
  styleUrls: ['./select-columns.component.scss']
})
export class SelectColumnsComponent implements OnInit {

  selectedColumns: string[] = [];
  moveColumns: string[] = [];
  columnNames: string[] = [];
  selected_trans_name: string | undefined;
  column_name1 = '';
  column_name2 = '';
  dropdownColsArr: any = [];
  newColsArr: any;
  enterNewColValueArr: any;
  enterNewColValue: any;
  userInput: string = '';
  enterInputValue: string = '';
  selectedFile: any = '';
  pattern = "";
  transformationNameOperation = {
    'join both names': {
      'existingCols': ['Column-1', 'Column-2'],
      'newCols': ['Enter New Column']
    },
    'formatting names': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'datetimeevent': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'dateformatting': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'gettingnumber': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'gettingtext': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'hardcodedvalue': {
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'incident changes': {
      'existingCols': ['Column-1'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'evnttypeEnviNearmischanges': {
      'existingCols': ['Column-1'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'evntsubtypetruevalue': {
      'existingCols': ['Column-1'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'getfatcatchanges': {
      'existingCols': ['Column-1'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'replacingvalue': {
      'existingCols': ['Column-1'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'combinereplacevalue': {
      'existingCols': ['Column-1', 'Column-2'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'duplicatechange': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'evntsubtypeProprtyRegulatorieschange': {
      'existingCols': ['Column-1', 'Column-2'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'commachange': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'treatmentchange': {
      'existingCols': ['Column-1'],
      'existingInputs': ['Enter Input Values'],
      'newCols': ['Enter New Column']
    },
    'changepolice': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'datefunctions': {
      'existingCols': ['Column-1', 'Column-2'],
      'newCols': ['Enter New Column']
    },
    'timechanges': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    },
    'datatypechange': {
      'existingCols': ['Column-1'],
      'newCols': ['Enter New Column']
    }
  }
  tooltipData = '';
  filteredCols1: string[] = [];
  col_names1: any = [];
  filteredCols2: string[] = [];
  col_names2: string[] = [];
  col_1_dropdown = [];
  fileNames: any;
  selectedfiles: any = [];
  index: any;
  variables1: any;
  variables2: any;
  filteredList1: any;
  filteredList2: any;
  allFilesData: any = [];
  selectedFileNames: any = [];
  constructor(
    public dialogRef: MatDialogRef<SelectColumnsComponent>, public fileuploadService: FileuploadService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }


  ngOnInit(): void {
    this.columnNames = [...this.data[0], ...this.fileuploadService.savedFileCols]
    this.allFilesData = [...this.data[2], ...this.fileuploadService.saveFileObj];
    for (let i = 0; i < this.allFilesData.length; i++) {
      this.selectedFileNames.push(this.allFilesData[i].name);
    }
    console.log(this.allFilesData, "alllll")
    this.selected_trans_name = this.data[1];
    if (this.fileuploadService.transFile.length != 0) {
      this.fileNames = this.fileuploadService.transFile;
      this.fileuploadService.transFile = [];

    }
    else {
      this.fileNames = [...this.data[2], ...this.fileuploadService.saveFileObj];

    }
    for (let i = 0; i < this.fileNames.length; i++) {
      this.selectedfiles.push(this.fileNames[i].name);
    }

    for (let [key, value] of Object.entries(this.transformationNameOperation)) {
      if (key == this.selected_trans_name) {
        for (let [key, val] of Object.entries(value)) {
          if (key == 'existingCols') {
            this.dropdownColsArr = val;
          }
          else if (key == 'newCols') {
            this.newColsArr = val;
          }
          else if (key == 'existingInputs') {
            this.enterNewColValueArr = val;
          }
          for (let [key, value] of Object.entries(val)) {
          }
        }
      }
    }

    switch (this.selected_trans_name) {
      case 'incident changes':
        this.tooltipData = "eg:'A','B'"
        this.pattern = "\'.*\',\'.*\'"
        break;
      case 'replacingvalue':
        this.tooltipData = " eg: {'key':'value','key':'value'.....}"
        break;
      case 'hardcodedvalue':
        this.tooltipData = "eg: 'value'"
        this.pattern = "\'.*\'"

        break;
      case 'treatmentchange':
        this.tooltipData = "eg: 'A','B','C','D'"
        this.pattern = "\'.*\',\'.*\',\'.*\',\'.*\'"

        break;
      case 'evntsubtypetruevalue':
        this.tooltipData = "eg: 'value'"
        this.pattern = "\'.*\'"

        break;
      case 'combinereplacevalue':
        this.tooltipData = "eg: {'key':'value','key':'value'.....}"
        // this.pattern = "\{'.*\':'.*\',*}"

        break;
      case 'evnttypeEnviNearmischanges':
        this.tooltipData = "eg:'value'"
        this.pattern = "\'.*\'"

        break;
      case 'getfatcatchanges':
        this.tooltipData = "eg:'value'"
        this.pattern = "\'.*\'"

        break;
      case 'evntsubtypeProprtyRegulatorieschange':
        this.tooltipData = "eg:'value'"
        this.pattern = "\'.*\'"

        break;
      default:
        break;
    }
  }

  valuechange() {
    this.fileuploadService.testSavedFile = this.selectedFile.name;
    this.index = this.selectedFileNames.indexOf(this.selectedFile.name)
    this.fileuploadService.uploadIndex = this.index;
    this.variables1 = this.columnNames[this.index];
    this.variables2 = this.columnNames[this.index];
    this.filteredList1 = this.variables1.slice();
    this.filteredList2 = this.variables2.slice();

  }
  onSubmit() {
    let transName = [];
    let columnList = [];
    let newColumnList: any
    let userInputList = [];
    let enterInputValuesList = [];

    transName.push(this.selected_trans_name);
    if (this.column_name1 != '') {
      columnList.push(this.column_name1);
    }
    if (this.column_name2 != '') {
      columnList.push(this.column_name2);
    }
    if (this.enterNewColValue != '') {
      newColumnList = this.enterNewColValue;
    }
    if (this.userInput != '') {
      userInputList.push(this.userInput);
    }
    if (this.enterInputValue) {
      enterInputValuesList.push(this.enterInputValue);
    }
    this.fileuploadService.transFile.push(this.selectedFile);
    // this.fileuploadService.transFile = [];
    this.dialogRef.close([transName, columnList, newColumnList, enterInputValuesList, this.selectedFile]);

  }
  onCancel() {
    this.dialogRef.close();
  }
  checkBoxvalue(event: string) {
    let val = event;
    this.selectedColumns.push(val);
  }
  deleteCol(evt: any) {
    let index = evt;
    this.selectedColumns.splice(index, 1);
  }
  moveSelectedCoumns() {
    this.moveColumns = this.selectedColumns;
  }
  removeSelectedCoumns() {

  }
}
