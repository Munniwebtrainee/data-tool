import { AfterViewInit, Component, Inject, OnInit, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-help-dialog',
  templateUrl: './help-dialog.component.html',
  styleUrls: ['./help-dialog.component.scss']
})
export class HelpDialogComponent implements OnInit {
  transNames = [{ name: 'Join Both Names', id: 'joinbothnames' },
  { name: 'formatting Names', id: 'formattingnames' },
  { name: 'datetimeevent', id: 'datetimeevent' },
  { name: 'gettingnumber', id: 'gettingnumber' },
  { name: 'gettingtext', id: 'gettingtext' },
  { name: 'hardcodedvalue', id: 'hardcodedvalue' },
  { name: 'incidentchanges', id: 'incidentchanges' },
  { name: 'evnttypeEnviNearmischanges', id: 'evnttypeEnviNearmischanges' },
  { name: 'evntsubtypetruevalue', id: 'evntsubtypetruevalue' },
  { name: 'getfatcatchanges', id: 'getfatcatchanges' },
  { name: 'replacingvalue', id: 'replacingvalue' },
  { name: 'combinereplacevalue', id: 'combinereplacevalue' },
  { name: 'duplicatechange', id: 'duplicatechange' },
  { name: 'evntsubtypeProprtyRegulatorieschange', id: 'evntsubtypeProprtyRegulatorieschange' },
  { name: 'commachange', id: 'commachange' },
  { name: 'treatmentchange', id: 'treatmentchange' },
  { name: 'changepolice', id: 'changepolice' },
  { name: 'datefunctions', id: 'datefunctions' },
  { name: 'timechanges', id: 'timechanges' },
  { name: 'datatypechange', id: 'datatypechange' }];



  activetext: any;

  constructor(
    public dialogRef: MatDialogRef<HelpDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private elementRef: ElementRef
  ) { }
  ngOnInit(): void {
    console.log(this.data);
  }

  onCancel() {
    this.dialogRef.close();
  }
  setStateAsActive(id: string) {
    this.activetext = id;
    this.scrollToElementById(id);
  }

  scrollToElementById(id: string) {
    const element = this.__getElementById(id);
    this.scrollToElement(element);
  }

  private __getElementById(id: string): HTMLElement {
    // const element = <HTMLElement>document.querySelector(`#${id}`);
    const element: any = document.getElementById(id);
    return element;
  }

  scrollToElement(element: HTMLElement) {
    element.scrollIntoView({ behavior: "smooth" });
  }
}
